#ifndef _GAMERPGSTATE
#define _GAMERPGSTATE

#include "../Kuko/states/IState.hpp"
#include "../Kuko/base/Sprite.hpp"
#include "../Kuko/entities/Map.hpp"
#include "../Kuko/entities/BaseEntity.hpp"
#include "../Kuko/widgets/UILabel.hpp"
#include "../entities/Player.hpp"
#include "../entities/Item.hpp"

class GameRpgState : public kuko::IState
{
    public:
    //IState() { m_isDone = false; m_isSetup = false; m_gotoState = ""; }
    virtual ~GameRpgState();

    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw();

    //virtual std::string GetNextState() { return m_gotoState; }

    protected:
    //bool m_isDone;
    //bool m_isSetup;
    //std::string m_gotoState;

    Player m_player;
    kuko::Map m_map;
    int m_timeout;
    int m_chanceForBattle;
};

#endif

