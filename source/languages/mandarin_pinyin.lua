language = {
	suggested_font = "NotoSansCJKtc-Regular.otf",
	
    -- English (key) / Translation
	game_title = "Jiù ná shùzhī",
	game_author = "Moosader Games'",
	game_version = "Bǎnběn 2.0",
	
	menu_play = "Ludi",
	menu_options = "Agordoj",
	menu_classic = "Tradicia",
	menu_enemy = "Fihundo",
	menu_rpg = "Rolludo",
	
	score = "Score",
	
	menu_play = "Wán",
	menu_options = "Shèzhì",
	menu_classic = "Chuántǒng",
	menu_enemy = "Huài gǒu",
	menu_rpg = "Juésè bànyǎn",
	
	hp = "HP"
}


