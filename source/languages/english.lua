language = {
	suggested_font = "PressStart2P.ttf",
	
    -- English (key) / English
	game_title = "Pickin' Sticks LXXIV",
	game_author = "Moosader Games'",
	game_version = "v2.0",
	
	menu_play = "Play",
	menu_options = "Options",
	menu_classic = "Classic",
	menu_enemy = "Bad Dog",
	menu_rpg = "RPG",
	
	score = "Score",

	battle_attack = "Attack",
	battle_defend = "Defend",
	battle_flee = "Flee",
	
	hp = "HP"
}


