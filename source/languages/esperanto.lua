language = {
	suggested_font = "PressStart2P.ttf",
	
    -- English (key) / Esperanto
	game_title = "Prenu Branĉojn 74",
	game_author = "Ludo de Moosader",
	game_version = "v2.0",
	
	menu_play = "Ludi",
	menu_options = "Agordoj",
	menu_classic = "Tradicia",
	menu_enemy = "Fihundo",
	menu_rpg = "Rolludo",
	
	score = "Poentoj",

	battle_attack = "Ataki",
	battle_defend = "Defendi",
	battle_flee = "Eskapi",
	
	hp = "Sano"
}
