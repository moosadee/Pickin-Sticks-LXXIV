#include "Player.hpp"

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/base/Application.hpp"

Player::Player()
{
    m_speed = 0.25;
    m_frameCounter = 0;
    m_isSolid = true;
    m_hp = 0;
    m_level = 1;
}

void Player::IncrementScore()
{
    m_score += 1;
}

void Player::DecrementScore()
{
    m_score -= 1;
}

int Player::GetScore()
{
    return m_score;
}

int Player::GetHP()
{
    return m_hp;
}

int Player::GetLevel()
{
    return m_level;
}
