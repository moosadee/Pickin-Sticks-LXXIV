elements = {
	{ 
		ui_type = "image", 
		id = "background", 
		x = 0, y = 0, width = 320, height = 480, 
		texture_id = "titlebg" 
	},
	
	{ 
		ui_type = "button", 
		id = "btnPlayClassic", text_id = "menu_classic", 
		x = 10, y = 300, width = 300, height = 50, 
		texture_id = "uibutton", 
		font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255,
		pad_x1 = 14, pad_x2 = 100, pad_y1 = 14, pad_y2 = 14
	},
	
	{ 
		ui_type = "button", 
		id = "btnPlayEnemy", text_id = "menu_enemy", 
		x = 10, y = 360, width = 300, height = 50, 
		texture_id = "uibutton", 
		font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255,
		pad_x1 = 14, pad_x2 = 100, pad_y1 = 14, pad_y2 = 14
	},
	
	{ 
		ui_type = "button", 
		id = "btnPlayRpg", text_id = "menu_rpg", 
		x = 10, y = 420, width = 300, height = 50, 
		texture_id = "uibutton", 
		font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255,
		pad_x1 = 14, pad_x2 = 65, pad_y1 = 14, pad_y2 = 14
	},
	
	{ 
		ui_type = "label", id = "title", 
		text_id = "game_title", 
		x = 10, y = 25, width = 300, height = 25, centered_text = 1,
		font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255,
	},
	
	{ 
		ui_type = "label", id = "moosader", 
		text_id = "game_author", 
		x = 10, y = 5, width = 170, height = 16, 
		font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255,
	},
	
	{ 
		ui_type = "label", id = "version", 
		text_id = "game_version", 
		x = 260, y = 50, width = 50, height = 15, 
		font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255,
	}
}
